import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
    render() {
      return (
        <Html>
          <Head />
          <body>
            <Main />
            <div id="modal" />
            <NextScript />
            <script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/7ccb1a3a-e907-4c2c-a143-6f3b3199ccb1-loader.js" ></script>
          </body>
        </Html>
      )
    }
  }