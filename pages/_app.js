import { ThemeProvider, createGlobalStyle } from 'styled-components'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import theme from '../src/styles/Themes/main'
import Head from 'next/head'
import Header from '../src/screens/Layout/Header/'
import Footer from '../src/screens/Layout/Footer/'

const GlobalStyle = createGlobalStyle`
  * {
    font-family: "Open Sans" sans-serif;
  }

  html, body {
    width: 100vw;
    padding: 0;
    margin: 0;
    box-sizing: border-box;
  }
`;

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <Head>
        <title>3Tricia</title>
        <link rel="preconnect" href="https://fonts.gstatic.com"></link>
        <link href="https://fonts.googleapis.com/css2?family=Assistant:wght@200;300;400;600&display=swap" rel="stylesheet"></link>
      </Head>
      <ThemeProvider theme={theme}>
        <Header />
        <Component {...pageProps} />
        <Footer />
      </ThemeProvider>
    </>
  )
}

export default MyApp
