import { useRouter } from 'next/router'

export default function (routeName) {
    const router = useRouter()
    const pushRoute = `${router.route}${routeName}`

    function handleFormularioProposta() {
        router.push(pushRoute)
    }

    return { handleFormularioProposta }
}