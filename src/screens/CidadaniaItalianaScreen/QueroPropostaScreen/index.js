import Input from '../../../styles/Input/'
import Text from '../../../styles/Text/'
import FormWrapper from '../../../styles/Form/'
import { FcInfo } from 'react-icons/fc'
import {
    Container,
    Form
} from 'react-bootstrap'

export default function QueroProposta() {
    return (
        <Container>
            <Text>
                Agora você irá responder um pequeno Quiz para saber a necessidade de quais serviços 
                você irá precisar contratar para realizar o reconhecimento da sua cidadania italiana!
            </Text>
            <FormWrapper>
                <Form>
                    <FormWrapper.InputGroup className="mb-3">
                        <FormWrapper.InputGroup.Prepend>
                            <Input placeholder="Nome" aria-label="Nome" />
                        </FormWrapper.InputGroup.Prepend>
                    </FormWrapper.InputGroup>
                    <FormWrapper.InputGroup className="mb-3">
                        <FormWrapper.InputGroup.Prepend>
                            <Input placeholder="E-mail" aria-label="E-mail" />
                        </FormWrapper.InputGroup.Prepend>
                    </FormWrapper.InputGroup>
                    <FormWrapper.InputGroup className="mb-3">
                        <FormWrapper.InputGroup.Prepend>
                            <Input placeholder="Telefone" aria-label="Telefone" />
                        </FormWrapper.InputGroup.Prepend>
                    </FormWrapper.InputGroup>
                    <FormWrapper.InputGroup className="mb-3">
                        <FormWrapper.InputGroup.Prepend>
                            <Input placeholder="Insira a quantidade de requerentes" aria-label="Insira a quantidade de requerentes" />
                        </FormWrapper.InputGroup.Prepend>
                    </FormWrapper.InputGroup>
                    <FormWrapper.InputGroup className="mb-3">
                        <FormWrapper.InputGroup.Prepend>
                            <Input placeholder="Quantos requerentes são menores de 14 anos?" aria-label="Quantos requerentes são menores de 14 anos?" />
                        </FormWrapper.InputGroup.Prepend>
                    </FormWrapper.InputGroup>
                </Form>
                <Text.Small className="pl-0">
                    <FcInfo className="mb-1 mr-1" size="17" />
                    Menores de 14 anos o requerimento é GRATUITO
                </Text.Small>
            </FormWrapper>
        </Container>
    )
}