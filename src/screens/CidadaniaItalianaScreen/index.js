import React from 'react'
import { Container } from 'react-bootstrap'
import ContentWrapper from './styles'
import usePushPropostaPage from '../../hooks/usePushPropostaPage'

export default function CidadaniaItalianaScreen() {

    const { handleFormularioProposta } = usePushPropostaPage('/quero-proposta')

    return(
        <Container className="mt-4">
            <ContentWrapper>
                <ContentWrapper.Title>
                Cidadania Italiana
                </ContentWrapper.Title>
                <ContentWrapper.Content>
                    In eiusmod adipisicing non aute elit do sunt do proident duis enim nulla voluptate aliquip. Ut minim enim amet aute commodo ut eu nulla consectetur. Ea tempor et consequat dolore et Lorem culpa elit adipisicing. Aliquip adipisicing nisi duis esse nostrud esse est sint sit sint deserunt sint sunt quis. Ex sint reprehenderit aliquip aute eu amet in qui excepteur non cupidatat consectetur pariatur.
                </ContentWrapper.Content>
                <ContentWrapper className="text-center">
                    <ContentWrapper.Button className="mt-4" onClick={handleFormularioProposta}>Quero uma Proposta!</ContentWrapper.Button>
                </ContentWrapper>         
            </ContentWrapper>
        </Container>
    )
}