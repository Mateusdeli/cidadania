import React from 'react'
import { Wrapper } from './styles'
import Header from './Header/'
import QuemSomos from './QuemSomos/'
import Carousel from './Carousel/'
import Equipe from './Equipe/'
import Certificados from './Certificados/'
import Servicos from './Servicos/'
import Midia from './Midia/'

export default function QuemSomosScreen() {
    return(
        <Wrapper>
          <Header />
          <QuemSomos />
          <Carousel />
          <Equipe />
          <Certificados />
          <Servicos />
          <Midia />
        </Wrapper>
    )
}