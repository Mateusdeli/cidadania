import styled from 'styled-components'
import { Container, Col } from 'react-bootstrap'
import Button from '../../../styles/Button/'

export const Wrapper = styled.div``;

export const TitleContent = styled.div`
    position: absolute;
    width: 463px;
    height: 56px;
    left: 390px;
    top: 702px;
`;

export const ParagraphContent = styled.div`
    position: absolute;
    width: 580px;
    height: 192px;
    left: 390px;
    top: 790px;
`;

export const ButtonInfo = styled(Button.Link)`
    position: absolute;
    left: 390px;
    top: 1070px;
    cursor: pointer;
`;

export const Image = styled.div`
    position: absolute;
    width: 483px;
    height: 360px;
    left: 1047px;
    top: 702px;
    background: url("images/quem-somos1.jpg");
    border-radius: 24px;
`;

export const WrapperButtons = styled.div`
    width: 106px;
    height: 41px;
`;

export const ButtonPrev = styled(Button.Arrow)`
    position: absolute;
    left: 1236px;
    top: 1086px;
`;

export const ButtonNext = styled(Button.Arrow)`
    position: absolute;
    left: 1301px;
    top: 1086px;
`;
