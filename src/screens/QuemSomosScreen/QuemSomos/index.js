import { Wrapper, TitleContent, ParagraphContent, Image, WrapperButtons, ButtonInfo, ButtonPrev, ButtonNext} from './styles'
import Link from 'next/link'
import Text from '../../../styles/Text/'

export default function QuemSomos() {
    return(
        <Wrapper>
            <TitleContent>
                <Text.Title size="40px" weight="700" line="56px">Quem somos</Text.Title>
            </TitleContent>
            <ParagraphContent>
                <Text size="16px" weight="400" line="32px">
                    O maior e mais confiável serviço de consultoria especializada para a EU e os EUA.
                    Na 3RICIAS, mais do que um cliente, você é parte da família. Com total transparência e mínima burocracia, você encontra conosco conforto e segurança para traçar o melhor caminho para ingressar no mercado europeu ou norte-americano. Cuidamos de todo o planejamento, papelada e burocracia, para que você possa conquistar o topo do mercado — seja no mundo comercial, ou no ramo pessoal e familiar.
                </Text>
            </ParagraphContent>
            {/* <Link href="#">
                {
                  <ButtonInfo width="173px" height="40px">
                      Quero conhecer!
                  </ButtonInfo>
                }
            </Link> */}
            <Image />
            <WrapperButtons>
                <ButtonPrev direction="left" />
                <ButtonNext direction="right" />
            </WrapperButtons>
        </Wrapper>
    )
}