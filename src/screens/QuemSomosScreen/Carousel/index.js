import { Wrapper, Image } from './styles'

export default function Carousel() {
    return (
        <Wrapper>
            <Image />
        </Wrapper>
    )
}