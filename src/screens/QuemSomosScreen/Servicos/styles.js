import styled from 'styled-components'

export const Wrapper = styled.div`
    position: absolute;
    width: ${({ theme }) => theme.root.home.width};
    height: 800px;
    left: ${({ theme }) => theme.root.left}px;
    top: 2997px;
    text-align: center;
    margin: 0 auto;
`;

Wrapper.Content = styled.div`
    width: 1140px;
`;

Wrapper.Cards = styled(Wrapper.Content)`
    display: flex;
    justify-content: space-around;
    margin-top: 64px;
`;
