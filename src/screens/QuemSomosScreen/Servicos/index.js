import { Wrapper } from './styles'
import Text from '../../../styles/Text/'
import Card from '../../../components/Card/'

export default function Servicos() {
    return (
        <Wrapper>
            <Wrapper.Content>
                <Text.Title weight="700" size="40px" line="56px">Podemos ajudar você a alcançar a <br /> tão sonhada cidadania!</Text.Title>
                <Text className="pt-3">Assessoramos você durante todo processo para sua cidadania Europeia.</Text>
            </Wrapper.Content>
            <Wrapper.Cards>
                <Card 
                  image="stylish-young-woman-sits-background-famous-ponte-vecchio-with-river-arno-florence-italy.png"
                  flag="438252c20c93d3963bd0876a9709fd611.png"
                  title="Cidadania Italiana"
                  description="Dupla cidadania Italiana? Conheça nossos serviços."
                />
                <Card 
                  image="happy-people-portugal.png"
                  flag="bandeira-portugal-1200x675.png"
                  title="Cidadania Portuguesa"
                  description="Dupla cidadania portuguesa? Conheça nossos serviços."
                />
            </Wrapper.Cards>
        </Wrapper>
    )
}