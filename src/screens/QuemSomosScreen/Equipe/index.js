import { Wrapper, Title, TextInfo } from './styles'
import CardEquipe from '../../../components/CardEquipe/'

export default function Equipe() {
    const icons = ['facebook.png', 'instagram.png', 'linkedin.png'];
    return(
        <Wrapper>
            <Wrapper.Content>
                <Title>
                    Nossa equipe
                </Title>
                <TextInfo line="32px" weight="400">A 3RICIAS é formada por consultores e prestadores de serviços qualificados que, de forma coordenada, inteligente e criativa oferecem, com eficácia serviços para sua nova vida como Cidadão Europeu.</TextInfo>
            </Wrapper.Content>
            <Wrapper.Cards>
                <CardEquipe icons={icons} name="Tricia Esteves Cesar" role="CEO e Fundadora" image="quem-somos5.png" />
                <CardEquipe name="Fernanda Botelho" role="Partner - Closer" image="quem-somos6.png" />
            </Wrapper.Cards>
        </Wrapper>
    )
}