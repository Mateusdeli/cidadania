import styled from 'styled-components'
import Text from '../../../styles/Text/'

export const Wrapper = styled.div`
    position: absolute;
    width: ${({ theme }) => theme.root.home.width};
    height: 515px;
    left: ${({ theme }) => theme.root.left}px;
    top: 1905px;
`;

Wrapper.Content = styled.div`
    display: flex;
    align-items: center;
    width: 1140px;
`;

Wrapper.Cards = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 64px;
    margin-bottom: 111px;
`;

export const Title = styled(Text.Title)`
    width: 50%;
`;

export const TextInfo = styled(Text)`
    margin-bottom: 0;
    font-family: "Open Sans" sans-serif;
`;

