import BackgroundMask from '../../../styles/BackgroundMask/'
import { Background, CarouselWrapper, Elipse, InfoContent } from './styles'
import Text from '../../../styles/Text/'
import Button from '../../../styles/Button/'
import Link from 'next/link'

export default function HomeCarousel() {
    return (
        <>
        <CarouselWrapper controls={false}>
            <CarouselWrapper.Item>
                <Background className="img-fluid" img="roman-coliseum-seen-from-afar.jpg">
                    <BackgroundMask image="shadow.png">
                        <InfoContent>
                            <Link href="#">
                                {<a>
                                    <InfoContent.Button>GO ITALIA!</InfoContent.Button>
                                </a>}
                            </Link>
                            <Text.Title size="40px" className="pt-2" color="#FFF" weight="700" line="52px">Tudo sobre a<br/> Cidadania Italiana</Text.Title>
                            <Text size="16px" color="#FFF" weight="400">Quer conquistar sua cidadania italiana e não sabe por onde começar? Nós da <br /> 3RICIAS estamos aqui para te ajudar nesta missão. Conheça nossos serviços.</Text>
                            <Link href="#">
                                {<a>
                                    <Button className="mt-4">Quero uma proposta</Button>
                                </a>}
                            </Link>
                        </InfoContent>
                        <InfoContent.Image src="images/italyhome.png" />
                    </BackgroundMask>
                </Background>
            </CarouselWrapper.Item>
            <CarouselWrapper.Item>
                <Background className="img-fluid" img="portugalbanner.png">
                    <BackgroundMask image="shadow.png">
                        <InfoContent>
                            <Link href="#">
                                {<a>
                                    <InfoContent.Button>GO PORTUGAL!</InfoContent.Button>
                                </a>}
                            </Link>
                            <Text.Title size="40px" className="pt-2" color="#FFF" weight="700" line="52px">Tudo sobre a<br/> Cidadania Portuguesa</Text.Title>
                            <Text size="16px" color="#FFF" weight="400">Você tem direito a Cidadania Portuguesa e não sabe por onde começar? Nós <br/> da 3RICIAS contamos com profissionais qualificados. O futuro é agora, entre <br/> em contato com a 3RICIAS e realize os seus sonhos.</Text>
                            <Link href="#">
                                {<a>
                                    <Button className="mt-4">Quero uma proposta</Button>
                                </a>}
                            </Link>
                        </InfoContent>
                        <InfoContent.Image/>
                    </BackgroundMask>
                </Background>
            </CarouselWrapper.Item>
        </CarouselWrapper>
        <Elipse />
        </>
    )
}