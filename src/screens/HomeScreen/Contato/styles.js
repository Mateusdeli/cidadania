import styled from 'styled-components'

export const Wrapper = styled.div`
    width: 1920px;
    height: 749px;
`;

export const Background = styled.div`
    position: absolute;
    left: 0px;
    top: 2620px;
    background: url("images/view-lisbon-cityscape-saint-jorge-castle.png");
    &::after {
        content: '';
        display: block;
        width: 1920px;
        height: 749px;
        background: url("images/backgroundyellow.png");
    }
`;

export const Icon = styled.img`
    padding-right: 16px;
`;

export const MapaContent = styled.div``;

export const List = styled.ul`
    list-style: none;
    padding-left: 0;
`;

List.Divisor = styled.hr`
    width: 100%;
    padding: 0;
`;

List.Item = styled.li`
    .spanAdjust {
        padding-left: 32px;
    }
`;