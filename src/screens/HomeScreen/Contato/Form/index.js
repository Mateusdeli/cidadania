import Input from '../../../../components/Input/'
import { Form } from '../../../../styles/Form/'
import { ButtonContainer, TextTermos, TermosContent } from './styles'
import Button from '../../../../styles/Button/'

export default function FormContato() {
    return (
        <Form className="pt-2">
            <Form.Group>
                <Form.Label weight="bold" size="16px" line="24px">Nome completo</Form.Label >
                <Input width="519" />
            </Form.Group>
            <Form.GroupInline>
                <Form.Group className="pr-3">
                    <Form.Label >E-mail</Form.Label>
                    <Input width="250" />
                </Form.Group>
                <Form.Group className="mr-4">
                    <Form.Label>Telefone</Form.Label>
                    <Input width="250" />
                </Form.Group>       
            </Form.GroupInline>
            <Form.Group>
                <Form.Label>Mensagem</Form.Label>
                <Form.Textarea height="132" width="100%" />
            </Form.Group>
            <Form.Group className="pt-3">
                <TermosContent>
                    <Input type="checkbox" width="15" />
                    <TextTermos>Sim, estou de acordo com os <strong>termos de uso.</strong></TextTermos>
                </TermosContent>
            </Form.Group> 
            <ButtonContainer>
                <Button width="193px" height="40px">Enviar!</Button>
            </ButtonContainer>
        </Form>
    )
}