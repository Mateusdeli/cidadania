import styled from 'styled-components'

export const ButtonContainer = styled.div`
    margin-top: 32px;
    float: right;
`;

export const TermosContent = styled.div`
    display: flex;
    align-items: center;
`;

export const TextTermos = styled.span`
    font-weight: 400;
    font-size: 16px;
    line-height: 16px;
    padding-left: 15px;
    font-family: "Open Sans" sans-serif;
`;