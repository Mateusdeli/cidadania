import { Background,  Wrapper, Icon, List, MapaContent} from './styles'
import FormContato from './Form/'
import Modal from '../../../styles/Modal/'
import Text from '../../../styles/Text/'
import Link from 'next/link'

export default function Contato() {
    return(
        <>
            <Wrapper>
                <Background />
                <Modal top="2499px" left="390px" width="600px" height="645px">
                    <Modal.Content paddingTop="52px" paddingLeft="40px" paddingBottom="41px" paddingRight="40px">
                        <Text.Title weight="700" size="28px" line="40px">Entre em contato</Text.Title>
                        <FormContato />
                    </Modal.Content>
                </Modal>
                <Modal top="2499px" left="1046px" width="484px" height="645px">
                    <List>
                        <List.Item>
                            <Modal.Content paddingTop="48px" paddingLeft="40px" paddingBottom="14px" paddingRight="40px">
                                <Icon className="img-fluid" src="images/locationIcon.png" />
                                <Text.Span size="18" font="Assistant" weight="400">Rua Nestor de Barros, 116 - Sala 65</Text.Span>
                                <br />
                                <Text.Span className="spanAdjust" size="18" font="Assistant" weight="400">Tatuapé - São Paulo / SP (CEP: 03325-050)</Text.Span>
                            </Modal.Content>
                        </List.Item>
                        <List.Divisor />
                        <List.Item>
                            <Modal.Content paddingTop="14px" paddingLeft="40px" paddingBottom="14px" paddingRight="40px">
                                <Icon className="img-fluid" src="images/email.png" />
                                <Text.Span className="p-0" size="18" font="Assistant" weight="400">contato@3riciascidadaniaitaliana.com.br</Text.Span>
                            </Modal.Content>
                        </List.Item>
                        <List.Divisor />
                        <List.Item>
                            <Modal.Content paddingTop="14px" paddingLeft="40px" paddingBottom="14px" paddingRight="40px">
                                <Icon className="img-fluid" src="images/phone.png" />
                                <Text.Span className="p-0" size="18" font="Assistant" weight="400">(11) 2384-2415</Text.Span>
                                <Icon className="img-fluid pl-5" src="images/whatsapp.png" />
                                <Text.Span className="p-0" size="18" font="Assistant" weight="400">(11) 94776-6841</Text.Span>
                            </Modal.Content>
                        </List.Item>
                        <List.Divisor />
                        <List.Item>
                            <Modal.Content paddingTop="14px" paddingLeft="40px" paddingBottom="14px" paddingRight="40px">
                                <Text size="14px" color="#737373" font="Assistant" weight="300" line="22px">Redes Socias</Text>
                                <Link href="#">
                                    {<a><Icon className="img-fluid" src="images/instagram.png" /></a>}
                                </Link>
                                <Link href="#">
                                    {<a><Icon className="img-fluid pl-2" src="images/facebook.png" /></a>}
                                </Link>
                                <Link href="#">
                                    {<a><Icon className="img-fluid pl-2" src="images/linkedin.png" /></a>}
                                </Link>
                            </Modal.Content>
                        </List.Item>
                        <Modal.Content paddingTop="14px" paddingLeft="40px" paddingBottom="14px" paddingRight="40px">
                            <MapaContent>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7314.860732422808!2d-46.559546!3d-23.552982000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5e867e61b7e5%3A0x5c3d91afbfe2cb12!2sR.%20Nestor%20de%20Barros%2C%20116%20-%20Vila%20Santo%20Estev%C3%A3o%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2003325-050!5e0!3m2!1spt-BR!2sbr!4v1613612602278!5m2!1spt-BR!2sbr" width="413" height="161" style={{border:0}} aria-hidden="false"></iframe>
                            </MapaContent>
                        </Modal.Content>
                    </List>
                </Modal>
            </Wrapper>
        </>
    ) 
}