import styled from 'styled-components'
import { Container, Col } from 'react-bootstrap'
import Text from '../../../styles/Text/'
import Button from '../../../styles/Button/'

export const Wrapper = styled.div``;

export const Title = styled(Text.Title)`
    position: absolute;
    width: 463px;
    height: 56px;
    left: 390px;
    top: 844px;
`;

export const Paragraph = styled(Text)`
    position: absolute;
    width: 580px;
    height: 192px;
    left: 390px;
    top: 932px;
`;

export const ButtonInfo = styled(Button.Link)`
    position: absolute;
    left: 390px;
    top: 1164px;
    cursor: pointer;
`;

export const Image = styled.div`
    position: absolute;
    width: 483px;
    height: 360px;
    left: 1047px;
    top: 844px;
    background: url("images/quem-somos1.jpg");
    border-radius: 24px;
`;

export const WrapperButtons = styled.div`
    position: absolute;
    width: 106px;
    height: 41px;
`;

export const ButtonPrev = styled(Button.Arrow)`
    position: absolute;
    left: 1236px;
    top: 1228px;
`;

export const ButtonNext = styled(Button.Arrow)`
    position: absolute;
    left: 1301px;
    top: 1228px;
`;
