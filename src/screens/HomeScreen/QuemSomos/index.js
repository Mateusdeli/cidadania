import { Wrapper, Title, Paragraph, Image, WrapperButtons, ButtonInfo, ButtonPrev, ButtonNext} from './styles'
import Link from 'next/link'

export default function QuemSomos() {
    return(
        <Wrapper>
            <Title size="40px" weight="700" line="56px">Quem somos</Title>
            <Paragraph size="16px" weight="400" line="32px">
                O maior e mais confiável serviço de consultoria especializada para a UE e os EUA.
                Na 3RICIAS, mais do que um cliente, você é parte da família. Com total transparência e mínima burocracia, 
                você encontra conosco conforto e segurança para traçar o melhor caminho para ingressar no mercado europeu ou norte-americano. 
                Cuidamos de todo o planejamento, papelada e burocracia, para que você possa conquistar o topo do mercado — seja no mundo comercial, 
                ou no ramo pessoal e familiar.
            </Paragraph>
            <div className="pt-3">
                <Link href="/quem-somos">
                    {
                    <ButtonInfo width="173px" height="40px">
                        Quero conhecer!
                    </ButtonInfo>
                    }
                </Link>
            </div>
            <Image />
            <WrapperButtons>
                <ButtonPrev direction="left" />
                <ButtonNext direction="right" />
            </WrapperButtons>
        </Wrapper>
    )
}