import HomeCarousel from './Carousel/'
import QuemSomos from './QuemSomos/'
import Servicos from './Servicos/'
import Contato from './Contato/'
import Wrapper from './styles'

export default function HomeScreen() {
    return (
      <>
        <Wrapper>
          <HomeCarousel />
          <QuemSomos />
          <Servicos />
          <Contato />
        </Wrapper>
      </>
    )
}