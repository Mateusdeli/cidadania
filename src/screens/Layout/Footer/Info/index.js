import { Container, Row, Col } from 'react-bootstrap'
import { TextName } from './styles'
import Text from '../../../../styles/Text/'
import Link from 'next/link'

export default function Info() {
    return(
        <Row className="justify-content-between">
            <Col>
                <Text>3RICIAS 2021 © Todos os direitos reservados.</Text>
            </Col>
            <Col className="text-center">
                <TextName>ZANGOLBIREVE</TextName>
            </Col>
            <Col className="text-right">
                <div>
                    <Link href="#">
                        {<a><img className="img-fluid pr-4" src="images/instagram.png" /></a>}
                    </Link>
                    <Link href="#">
                        {<a><img className="img-fluid pr-4" src="images/facebook.png" /></a>}
                    </Link>
                    <Link href="#">
                        {<a><img className="img-fluid" src="images/linkedin.png" /></a>}
                    </Link>
                </div>
            </Col>
        </Row>
    )
}