import { Container, Row, Col } from 'react-bootstrap'
import { Wrapper, List} from './styles'
import Button from '../../../../styles/Button/'
import Link from 'next/link'

export default function Contato() {
    return(
        <Wrapper>
            <div>
                <img className="img-fluid" src="images/logoblack.png" />
            </div>
            <List>
                <List.Item>
                    <Link href="#">
                    {<a>
                        <h4>Institucional</h4>
                    </a>}
                    </Link>
                </List.Item>
                <List.Item>
                    <Link href="#">{<a>Quem somos</a>}</Link>
                </List.Item>
                <List.Item>
                    <Link href="#">{<a>Serviços</a>}</Link>
                </List.Item>
                <List.Item>
                    <Link href="#">{<a>Cidadania Portuguesa</a>}</Link>
                </List.Item>
                <List.Item>
                    <Link href="#">{<a>Cidadania Italiana</a>}</Link>
                </List.Item>
            </List>
            <List>
                <List.Item><h4>Institucional</h4></List.Item>
                <List.Item>Quem somos</List.Item>
                <List.Item>Serviços</List.Item>
                <List.Item>Cidadania Portuguesa</List.Item>
                <List.Item>Cidadania Italiana</List.Item>
            </List>
            <div>
                <Button paddingX="41" paddingY="11">Quero uma proposta</Button>
            </div>
        </Wrapper>
    )
}