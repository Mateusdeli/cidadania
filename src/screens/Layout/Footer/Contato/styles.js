import styled from 'styled-components'
import { Container, Row, Col } from 'react-bootstrap'

export const Wrapper = styled.div`
    display: flex;
    justify-content: space-between;
    padding-top: 100px;
    padding-bottom: 64px;
`;

export const List = styled.ul`
    list-style: none;
`;

Wrapper.Row = styled(Row)`
    height: 410px;
`;

Wrapper.Column = styled(Col)`
    width: 80%;
`;

List.Item = styled.li`
    a {
        color: ${({ theme }) => theme.colors.black};
        text-decoration: none;
    }
`;