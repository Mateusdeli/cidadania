import styled from 'styled-components'
import rgba from '../../../utils/RgbaColor/'

export const HeaderBase = styled.header`
    position: absolute;
    z-index: 1001;
    width: 100%;
`;
