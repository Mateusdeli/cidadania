import React from 'react'
import Link from 'next/link'
import { Wrapper, Nav, Logo } from './styles'

export default function Navigation() {

  return (
   <>
     <Wrapper>
       <Nav>
          <Nav.List>
            <Nav.Item height="18" width="138" top="61" left="516">
              <Link href="/cidadania-portuguesa">Cidadania Portuguesa</Link>
            </Nav.Item>
            <Nav.Item height="18" width="114" top="61" left="702">
              <Link href="/cidadania-italiana">Cidadania Italiana</Link>
            </Nav.Item>
            <Nav.Item top="24" left="896">
                <Link href="/">
                  {
                    <a>
                      <Logo src="images/Logo.png" />
                    </a>
                  }
                </Link>
            </Nav.Item>
            <Nav.Item height="18" width="83" top="61" left="1104">
              <Link href="/quem-somos">Quem somos</Link>
            </Nav.Item>
            <Nav.Item height="18" width="52" top="61" left="1235">
              <Link href="/servicos">Serviços</Link>
            </Nav.Item>
          </Nav.List>
       </Nav>
     </Wrapper>
   </>
  )
}