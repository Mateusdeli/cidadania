import styled from 'styled-components'
import Link from 'next/link'

export const Wrapper = styled.div``;
export const Nav = styled.nav``;

export const Logo = styled.img`
    height: 92px; 
    width: 128px;
`;

Nav.List = styled.ul`
    list-style: none;
`;
Nav.Item = styled.li`
    position: absolute;
    width: ${({ width }) => width}px;
    height: ${({ height }) => height}px;
    left: ${({ left }) => left}px;
    top: ${({ top }) => top}px;

    font-style: normal;
    font-weight: 600;
    font-size: 13px;
    line-height: 18px;

    a {
        color: ${({ theme }) => theme.colors.white};
        text-decoration: none;
    }
`;