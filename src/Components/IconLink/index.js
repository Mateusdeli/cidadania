import React, { forwardRef } from 'react'
import Link from 'next/link'
import { MdMailOutline } from 'react-icons/md'
import { FaInstagram, FaFacebookSquare } from 'react-icons/fa'

const components = {
    email: MdMailOutline,
    instagram: FaInstagram,
    facebook: FaFacebookSquare
};

export default function IconLink({ type, href,  color, size }) {
    const IconType = components[type]
    const IconComponent = forwardRef((props, ref) => {
        return (
            <a ref={ref} {...props}>
                <IconType title={type} color={color} size={size} />
            </a>
        );
    });
    return (
        <>
          <Link href={href}>
            <IconComponent />
          </Link>
        </>
    )
}
