import {  SelectField } from './styles'

export default function Select(props, { width, name}) {
    return (
        <SelectField name={name} width={width} {...props} />
    )
}