import styled from 'styled-components'

export const SelectField = styled.select`
   border: 1px solid #DFE1E4;
    border-radius: 6px;
    width: ${({ width }) => width}px;
    height: 48px;
    background: url("images/arrowselect.png") 95.5% 50% no-repeat;
    -webkit-appearance: none;
    -moz-appearance:    none;
    appearance:         none;
    padding-left: 5px;
    outline: 0;
`;