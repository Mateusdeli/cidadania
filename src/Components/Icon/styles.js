import styled from 'styled-components'

export const Icon = styled.img`
    padding-right: 16px;
`;
