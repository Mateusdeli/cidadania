import Text from '../../styles/Text/'
import { Icon } from './styles'

export default function Icon(props, { text, image }) {
    return(
        <>
            <Icon className="img-fluid" src={image} />
            <Text.Span {...props}>{text}</Text.Span>
        </>
    )
}

