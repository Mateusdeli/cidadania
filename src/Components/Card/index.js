import CardTemplate, { Flag, Title, Text, Arrow } from './styles'
import Link from 'next/link'

export default function Card(props) {
    return (
        <CardTemplate image={props.image}>
            <CardTemplate.Content>
                <CardTemplate.Infomation>
                    <Flag className="img-fluid" src={`images/${props.flag}`} />
                    <Title>{props.title}</Title>
                    <Text>{props.description}</Text>
                </CardTemplate.Infomation>
                <CardTemplate.Arrow>
                    <Link href="/">
                        { <Arrow /> }
                    </Link>
                </CardTemplate.Arrow>
            </CardTemplate.Content>
        </CardTemplate>
    )
}