import { InputField, InputCheckboxField } from './styles'

const types = {
    checkbox: 'checkbox'
}

export default function Input(props, { width, height, type, name}) {
    switch (props.type) {
        case types.checkbox:
            return (<InputCheckboxField type={type} name={name} height={height} width={width} {...props} />)

        default:
            return (<InputField type={type} name={name} height={height} width={width} {...props} />)
    }
}