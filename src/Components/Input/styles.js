import styled from 'styled-components'

export const InputField = styled.input`
    border: 1px solid ${({ theme }) => theme.colors.light};
    border-radius: 6px;
    width: ${({ width }) => width}px;
    height: ${({ height }) => height ? height : 48}px;
`;

export const InputCheckboxField = styled.input`
    all: unset;
    border: 1px solid ${({ theme }) => theme.colors.light};
    display: inline-block;
    color: ${({ theme }) => theme.colors.white};
    width: ${({ width }) => width ? width : 15}px;
    height: ${({ height }) => height ? height : 15}px;
    &:checked{
        background-color: ${({ theme }) => theme.colors.primary};
        color: ${({ theme }) => theme.colors.white};
        width: ${({ width }) => width ? width : 15}px;
        height: ${({ height }) => height ? height : 15}px;
        &::after {
            content: '';
            background: url("images/checkmark.png") no-repeat;
            background-position: center;
            width: 100%;
            height: 100%;
            display: block;
        }
    }
    
`;