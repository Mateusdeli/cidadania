import { Wrapper, Icon } from './styles'
import BackgroundMask from '../../styles/BackgroundMask/'
import Text from '../../styles/Text/'
import Link from 'next/link'

export default function CardEquipe(props) {
    const { image, name, role, icons } = props;
    return(
        <Wrapper {...props} image={image}>
            <BackgroundMask border="16px" image="equipe-background.png">
                <Wrapper.Content>
                    {icons && (
                        <Wrapper.Icons>
                            {icons.map((icon, index) => (
                                <Link key={index} href="#">
                                    {<Icon className="img-fluid" src={`images/${icon}`} />}
                                </Link>
                            ))}
                        </Wrapper.Icons>
                    )}
                    <Text.Title size="24px" weight="700">{name}</Text.Title>
                    <Text className="mb-0">{role}</Text>
                </Wrapper.Content>
            </BackgroundMask>
        </Wrapper>
    )
}