export default {
    root: {
        left: 390,
        home: {
            width: 1920,
            height: 3320,
        },
        quem_somos: {
            width: 1920,
            height: 4340
        }
    }, 
    primary: '#DBB71D',
    colors: {
        primary: '#DBB71D',
        grey: '#787878',
        lightgrey: '#F1F1F1',
        white: '#FFFFFF',
        black: '#232323',
        lightyellow: '#F9F4DB',
        light: '#DFE1E4'
    },
    fonts: {
        primary: "Open Sans",
        secondary: "Assistant"
    }
}