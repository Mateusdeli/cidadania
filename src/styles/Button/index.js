import styled from 'styled-components';

const Button = styled.button`
    background-color: ${({ theme }) => theme.colors.primary};
    width: ${({ width }) => width};
    height: ${({ height }) => height};
    padding: 11px 32px;
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    font-size: 13px;
    line-height: 18px;
    border-radius: 112px;
    border: none;
    outline: none;
    text-align: center;
    color: ${({ theme }) => theme.colors.white};
`;

Button.Link = styled.a`
    background-color: ${({ theme }) => theme.colors.primary};
    width: ${({ width }) => width};
    height: ${({ height }) => height};
    padding: 11px 32px;
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    font-size: 13px;
    line-height: 18px;
    border-radius: 112px;
    border: none;
    outline: none;
    text-align: center;
    color: ${({ theme }) => theme.colors.white};
    &:hover {
        text-decoration: none;
        color: ${({ theme }) => theme.colors.white};
    }
`;

Button.Outline = styled.a`
    background-color: ${({ theme }) => `rgba(${theme.colors.primary}, .8)`};
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 11px 32px;
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    font-size: 13px;
    line-height: 18px;
    width: ${({ width }) => width};
    height: ${({ height }) => height};
    border-radius: 112px;
    border: none;
    text-align: center;
    color: ${({ theme }) => theme.colors.white};
    &:hover {
        text-decoration: none;
        color: ${({ theme }) => theme.colors.white};
    }
`;

Button.Arrow = styled.button`
    border: none;
    outline: 0;
    background-color: rgba(219, 183, 29, 0.16);
    width:  ${({width}) => width ? width : 41}px;
    border-radius: 100%;
    height: ${({heigth}) => heigth ? heigth : 41}px;
    ${({ direction }) => direction === 'left' && 
        `background-image: url('./images/arrowleft.png');`
    }
    ${({ direction }) => direction === 'right' && 
        `background-image: url('./images/arrowright.png');`
    }
    background-repeat: no-repeat;
    background-position: center;
`;

export default Button