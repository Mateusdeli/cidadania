import styled from 'styled-components'

const Input = styled.input`
    width: 400px;
    min-width: 100%;
    max-width: 100%;
    background-color: ${({ theme }) => theme.colors.white};
    border-style: groove;
    border-radius: ${({ theme }) => theme.radius.big}px;
    padding: ${({ theme }) => theme.spacing.small}px ${({ theme }) => theme.spacing.big}px;
    outline: 0;
    box-shadow: 0 0 0 0;

    @media (max-width: 576px) {
        width: 300px;
    }
`

export default Input