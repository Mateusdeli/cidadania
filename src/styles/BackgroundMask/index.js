import styled from 'styled-components'

const BackgroundMask = styled.div`
    width: 100%;
    height: 100%;
    background: ${({ image }) => `url('images/${image}');`} no-repeat;
    border-radius: ${({ border }) => border ? border : 0};
`;

export default BackgroundMask